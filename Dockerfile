FROM openjdk:17 AS build
WORKDIR /app
COPY gradle ./gradle
COPY gradlew build.gradle.kts settings.gradle.kts ./
RUN ./gradlew --no-daemon build 2>&1 > /dev/null || true # cache dependencies in separate layer
COPY . ./
RUN ./gradlew --no-daemon build

FROM openjdk:17
COPY --from=build /app/build/libs/forms-*-SNAPSHOT.jar /app/app.jar
WORKDIR /app
EXPOSE 8080/tcp
CMD ["java", "-jar", "/app/app.jar"]

package no.dossier.forms

import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
class FormController(private val formRepository: FormRepository) {
    @GetMapping
    fun index(): Iterable<Form> = formRepository.findAll()

    @PostMapping
    fun createForm(@RequestBody form: Form): Form {
        form.fields.forEach { it.form = form }
        return formRepository.save(form)
    }

    @PutMapping
    fun updateForm(@RequestBody form: Form): Form {
        form.fields.forEach { it.form = form }
        return formRepository.save(form)
    }

    @GetMapping("clear")
    fun clear() {
        formRepository.deleteAll()
    }
}
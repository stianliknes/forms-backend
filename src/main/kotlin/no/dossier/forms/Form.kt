package no.dossier.forms

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.repository.CrudRepository
import javax.persistence.*

@Entity
class FormField(
    @Id @GeneratedValue
    var id: Long?,
    var name: String,
    var type: FormType,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name ="form_id")
    var form: Form?
)

@Entity
class Form(
    @Id @GeneratedValue val id: Long?,
    var name: String,

    @OneToMany(mappedBy = "form", fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    var fields: List<FormField>
)


enum class FormType {
    @JsonProperty("text") TEXT,
    @JsonProperty("email") EMAIL
}

interface FormRepository : CrudRepository<Form, Long>
